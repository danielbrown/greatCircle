use std::io;

fn main() {

    let location1 = location_setter();

    let location2 = location_setter();

    let delta_lat = (location1.latitude - location2.latitude).to_radians();
    let delta_long = (location1.longitude - location2.longitude).to_radians();

    let r_earth = 6371.0_f64;
    let central_angle_0 = (delta_lat / 2.0).sin().powi(2)
        + location1.latitude.to_radians().cos() * location2.latitude.to_radians().cos() * (delta_long / 2.0).sin().powi(2);
    let central_angle_1 = 2.0 * central_angle_0.sqrt().asin();
    let d = r_earth * central_angle_1;
    
    println!("The distance between {} and {} is {} km.", location1.name, location2.name, d);
}

fn double_get() -> f64 {
    let mut entry = String::new();

    io::stdin()
        .read_line(&mut entry)
        .expect("Failed to read line");
    
        let entry: f64 = entry.trim().parse().expect("Please type a number");

        return entry;
}

fn string_get() -> String {
    let mut entry = String::new();

    io::stdin()
        .read_line(&mut entry)
        .expect("Failed to read line");

        return entry;
}

fn location_setter() -> Location {
    println!("Enter the location of interest");
    let name = string_get();

    println!("Enter the latitude of location");
    let latitude = double_get();

    println!("Enter the longitude of location");
    let longitude = double_get();
    let location1 = Location {
        name: name,
        latitude: latitude,
        longitude: longitude,
    };
    return location1;
}

struct Location {
    name: String,
    latitude: f64,
    longitude: f64
}
